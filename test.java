package com.example.helloworld;

import android.content.Intent;
import android.icu.util.Output;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.lingala.zip4j.ZipFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipException;


public class SubActivity extends AppCompatActivity {



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub);
        TextView txt = this.findViewById(R.id.subText);

        Intent intent = getIntent();
        String word = intent.getStringExtra("message");

        if (word.equals("")) {
            txt.setText("No message");
        } else {
            txt.setText(word);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        writeFile("test.txt", "this is test".getBytes(StandardCharsets.UTF_8));
        String cachePath = getApplicationContext().getCacheDir().getPath();
        String zipPath = cachePath + "/test.zip";
        String filePath = cachePath + "/test.txt";
        extractZip(zipPath, cachePath, "hoge");
        String tmp = readFile(filePath);
        Log.i("test", "text: " + tmp);

    }


    public boolean writeFile(String filename, byte[] data) {

        File fileCache = new File(getApplicationContext().getCacheDir(), filename);
        OutputStream outputStream = null;
        boolean ret = true;

        try {
            //fileCache.createNewFile();
            outputStream = new FileOutputStream(fileCache);
            outputStream.write(data);
            outputStream.flush();
            Log.d("test", "write success");
        }catch (IOException ioe ){
            Log.e("test" , "Exception", ioe);
            ret = false;
        } finally {
            try {
                if(outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e ) {
                Log.e("test", "exception", e);
            }
        }

        return ret;
    }

    public boolean extractZip(String zipPath, String outDirPath, String password) {
        Log.i("test", "start extract Zip:" + zipPath);
        File file = new File(zipPath);
        boolean ret = true;
        if (!file.exists()) {
            Log.e("test", "not exist:"+ zipPath );
            return false;
        }

        try {
            new ZipFile(zipPath, password.toCharArray()).extractAll(outDirPath);
        } catch (net.lingala.zip4j.exception.ZipException e) {
            Log.e("test", "extractZip: failed", e);
            ret = false;
        }
        return ret;
    }

    public String readFile(String path) {

        File file = new File(path);
        if (!file.exists()) {
            Log.e("test", "not exist:" + path);
            return null;
        }

        Log.i("test", "readFile:" + path);
        StringBuffer sb = new StringBuffer();
        try {
            FileInputStream in = new FileInputStream(file);
            try (BufferedReader reader = new BufferedReader( new InputStreamReader(in, StandardCharsets.UTF_8));) {
                String tmp;
                while ((tmp = reader.readLine()) != null) {
                    sb.append(tmp);
                }
            }
        } catch (FileNotFoundException e) {
            // todo : error handling
        } catch (IOException e) {
            // todo : error handling
        }
        return sb.toString();
    }

}
