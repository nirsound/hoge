from django.db import models

# Create your models here.
class BoardModel(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    author = models.CharField(max_length=20)
    images = models.ImageField(upload_to='')
    goods = models.IntegerField()
    read = models.IntegerField()
    readtext = models.CharField(max_length=200)